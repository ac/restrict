// SPDX-FileCopyrightText: 2023 Antonino Catinello
// SPDX-License-Identifier: BSD-3-Clause

package restrict

import (
	"errors"
	"os"
)

// Compare allow
func equal(a, b []string) bool {
	if len(a) != len(b) {
		return false
	}

	for i, v := range a {
		if v != b[i] {
			return false
		}
	}

	return true
}

func contains(s []string, e string) bool {
	for _, a := range s {
		if a == e {
			return true
		}
	}

	return false
}

func exists(path string) bool {
	_, err := os.Stat(path)
	return !errors.Is(err, os.ErrNotExist)
}

func deduplicate(p []permissions) []permissions {
	var r []permissions
	m := make(map[string]permissions)

	for _, x := range p {
		v, ok := m[x.path]
		if ok {
			if v.date.Before(x.date) {
				m[x.path] = x
			}
		} else {
			m[x.path] = x
		}
	}

	for _, x := range m {
		r = append(r, x)
	}

	return r
}
