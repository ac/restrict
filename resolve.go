// SPDX-FileCopyrightText: 2022 Antonino Catinello
// SPDX-License-Identifier: BSD-3-Clause

package restrict

import (
	"context"
	"crypto/tls"
	"errors"
	"net"
	"regexp"
	"strconv"
	"strings"
	"time"

	"catinello.eu/inet"
)

var (
	// ResolverPrefer indicates an IP type to resolve to, if a dns name is passed to Resolver().
	ResolverPrefer IP = IPv6
	// With DNS over TLS acitvated your DNS server needs a certificate that is valid in the CA pool for the destination IP address.
	ResolverTLSVerify bool = true
	// ResolverDialer enables you to set your Timeouts/Deadlines as wanted.
	ResolverDialer *net.Dialer = &net.Dialer{Timeout: time.Second * 2}
)

type proto uint8

const (
	// Resolver DNS connection types
	UDP proto = iota
	TCP
	TLS
)

func (p proto) String() string {
	switch p {
	case UDP:
		return "UDP"
	case TCP:
		return "TCP"
	case TLS:
		return "TLS"
	}

	return ""
}

// Internet Protocol
type IP uint8

// Possible IP versions to set.
const (
	None IP = iota
	IPv4
	IPv6
)

func kind(host string) IP {
	v4, err := regexp.MatchString(`^((25[0-5]|(2[0-4]|1\d|[1-9]|)\d)\.?\b){4}$`, host)
	if v4 && err == nil {
		return IPv4
	}

	v6, err := regexp.MatchString(`^((([0-9A-Fa-f]{1,4}:){1,6}:)|(([0-9A-Fa-f]{1,4}:){7}))([0-9A-Fa-f]{1,4})$`, host)
	if (v6 && err == nil) || (strings.HasPrefix(host, "::") && len(host) > 2) || (strings.HasSuffix(host, "::") && len(host) > 2) {
		return IPv6
	}

	// Not an IP / probably hostname
	return None
}

type resolver struct {
	ip     net.IP
	port   string
	proto  proto
	dot    bool
	verify bool
}

// ResolverX takes a dns server (ip/hostname), a target port and a boolean to activate DNS over TLS to use TCP and your systems root CA (rpath / AccessCerts()) or given Trust() configuration and lastly a udp boolean indicator that is incompatible with the previous dot boolean. DOT wins if both are set to true.
//
// Using a hostname instead of an IP address triggers a workaround to determine an adequate IP for that host in the prefered (see ResolverPrefer global variable that defaults to IPv6) IP version via the systems nameservers (UDP/53) for that single lookup.
//
// TLS verification is only available for IP addresses, not for hostnames. If your DNS server's IP is not listed in the certficates X509v3 Subject Alternative Name, then you need to disable ResolverTLSVerify and verify your trust otherwise to establish a connection. You can skip verify via global variable ResolverTLSVerify to set as false.
//
// Most public dns providers are equipped like dns.quad9.net, dns.sb, dns0.eu (only IPv4), one.one.one.one, dns.google, dns.opendns.com.
func ResolverX(dns, port string, dot, udp bool) error {
	err := caller("ResolverX")
	if err != nil {
		return err
	}

	var ip net.IP

	if kind(dns) == None {
		ips, err := net.LookupIP(dns)
		if err != nil {
			return err
		}

		if len(ips) == 0 {
			return errors.New("Given name did not resolve to an IP address.")
		}

		if state.GetBool("debug") {
			logger.Println("DNS Server IP's: ", ips)
		}

		if ResolverPrefer != None {
			// switch default prefer if not usable
			if !inet.HasIPv6() {
				ResolverPrefer = IPv4
			}

			if !inet.HasIPv4() {
				ResolverPrefer = IPv6
			}

			for n, x := range ips {
				if kind(x.String()) == ResolverPrefer {
					ip = ips[n]
					break
				}
			}

			// fallback if no prefered found
			if ip == nil {
				if state.GetBool("debug") {
					logger.Println("No suitable DNS IP address for prefered IP version found, resort to fallback: ", ips[0])
				}
				ip = ips[0]
			}
		} else {
			ip = ips[0]
		}
	} else {
		ip = net.ParseIP(dns)
		if ip == nil {
			return errors.New("Given DNS Server is not a valid IP address.")
		}
	}

	var pr proto

	if dot && !udp {
		pr = TLS
	}
	if !dot && !udp {
		pr = TCP
	}
	if !dot && udp {
		pr = UDP
	}

	state.Set("dns", dns+":"+port)
	state.Set("dnsIP", ip)
	state.Set("dnsProto", pr)

	if state.GetBool("debug") {
		logger.Println("DNS Server IP address to use: ", ip)
	}

	p, err := strconv.Atoi(port)
	if err != nil {
		return errors.New("Given port is invalid.")
	}

	if p < 1 || p > 65535 {
		return errors.New("Given port is invalid.")
	}

	r := &resolver{ip: ip, port: port, dot: dot, proto: pr, verify: ResolverTLSVerify}
	return r.resolver()
}

// Resolver takes an IP address or name of a DNS server and sets it as the DefaultResolver.
//
// Calls are processed via UDP on port 53 with the given IP version (see ResolverPrefer). It is an alias for ResolverX(dns, "53", false, true).
//
// A more advanced approach for usage with non-standard ports or TLS connections that can be accomplished via ResolverX.
//
// This mainly targets standard library functions.
func Resolver(dns string) error {
	err := caller("Resolver")
	if err != nil {
		return err
	}

	return ResolverX(dns, "53", false, true)
}

// ResolverTCP takes an IP address of a DNS server and sets it as the DefaultResolver.
// Calls are processed via TCP on port 53 with the given IP version (see ResolverPrefer).
// It is an alias for ResolverX(dns, "53", false, false).
func ResolverTCP(dns string) error {
	err := caller("ResolverTCP")
	if err != nil {
		return err
	}

	return ResolverX(dns, "53", false, false)
}

// ResolverTCP takes an IP address of a DNS server and sets it as the DefaultResolver.
// Calls are processed via TCP on port 853 with the given IP version (see ResolverPrefer).
// You can control the TLS verification via ResolverTLSVerify global variable.
// It is an alias for ResolverX(dns, "853", true, false).
func ResolverTLS(dns string) error {
	err := caller("ResolverTLS")
	if err != nil {
		return err
	}

	return ResolverX(dns, "853", true, false)
}

// resolver sets the default resolver in net.
func (d *resolver) resolver() error {
	if state.GetBool("disable") {
		return nil
	}

	// Was Syscalls already called and dns is not set, then return error.
	if state.GetBool("isFirstSyscalls") == false && state.GetBool("dnsIsSet") == false {
		return errors.New("restrict.Resolver needs 'dns' as set if Syscalls() restricted.")
	}

	switch d.proto {
	case TLS:
		net.DefaultResolver = d.tls()
	case TCP:
		net.DefaultResolver = d.tcp()
	case UDP:
		net.DefaultResolver = d.udp()
	}

	if state.GetBool("isFirstResolver") {
		state.Set("isFirstResolver", false)
	}

	return nil
}

func (d *resolver) udp() *net.Resolver {
	r := &net.Resolver{PreferGo: true, StrictErrors: true}
	ResolverDialer.Resolver = r
	r.Dial = func(ctx context.Context, network, address string) (net.Conn, error) {
		return ResolverDialer.DialContext(ctx, "udp", net.JoinHostPort(d.ip.String(), d.port))
	}

	return r
}

func (d *resolver) tcp() *net.Resolver {
	r := &net.Resolver{PreferGo: true, StrictErrors: true}
	ResolverDialer.Resolver = r
	r.Dial = func(ctx context.Context, network, address string) (net.Conn, error) {
		return ResolverDialer.DialContext(ctx, "tcp", net.JoinHostPort(d.ip.String(), d.port))
	}

	return r
}

func (d *resolver) tls() *net.Resolver {
	dialer := &tls.Dialer{
		Config: &tls.Config{
			InsecureSkipVerify: !d.verify,
		},
		NetDialer: ResolverDialer,
	}

	r := &net.Resolver{PreferGo: true, StrictErrors: true}
	dialer.NetDialer.Resolver = r
	r.Dial = func(ctx context.Context, network, address string) (net.Conn, error) {
		return dialer.DialContext(ctx, "tcp", net.JoinHostPort(d.ip.String(), d.port))
	}

	return r
}
