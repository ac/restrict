// SPDX-FileCopyrightText: 2024 Antonino Catinello
// SPDX-License-Identifier: BSD-3-Clause

// Make sure that there is a /etc/resolv.conf (can be empty / needs just to exists) if you need name resolution. OpenBSD requires that file for resolution and the net library enforces this requirement.⁴
//
//	⁴ https://cs.opensource.google/go/go/+/master:src/net/conf.go;l=301;drc=6536c207c2309da7c1c21e3669f8ddf491e31f5b
package restrict
