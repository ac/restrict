// SPDX-FileCopyrightText: 2024 Antonino Catinello
// SPDX-License-Identifier: BSD-3-Clause

//go:build linux || openbsd

package restrict

import (
	"testing"
)

func TestState(t *testing.T) {
	// test all
	err := Syscalls("stdio error proc rpath wpath cpath dpath tmppath inet mcast fattr chown flock dns getpw sendfd recvfd tape tty exec prot_exec settime ps vminfo id pf route wroute audio video bpf unveil capabilities landlock")
	if err != nil {
		t.Error(err)
	}

	State()
}
