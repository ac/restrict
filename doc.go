// SPDX-FileCopyrightText: 2024 Antonino Catinello
// SPDX-License-Identifier: BSD-3-Clause

// See the examples subfolder for usage.
//
// If you intend to chroot your process, you need to be aware of the following:
package restrict
