// SPDX-FileCopyrightText: 2022 Antonino Catinello
// SPDX-License-Identifier: BSD-3-Clause

//go:build openbsd

package restrict

import (
	"time"

	"golang.org/x/sys/unix"
)

// Access is an alias for unveil¹.
//
// ¹ https://man.openbsd.org/unveil.2
func Access(path string, flags string) error {
	err := caller("Access")
	if err != nil {
		return err
	}

	if state.GetBool("disable") {
		return nil
	}

	if state.GetBool("isFirstAccess") {
		state.Set("isFirstAccess", false)
	}

	a, _ := state.Get("access")
	a = append(a.([]permissions), permissions{date: time.Now(), path: path, flags: flags})
	state.Set("access", deduplicate(a.([]permissions)))

	return unix.Unveil(path, flags)
}

// AccessLock blocks further Access calls. You must call it once!
// If you call it without calling Access() before, then you will essentially disallow the whole filesystem.
//
// Beware! This differs in behaviour to the two NULL arguments unveil block to conform with Landlocks' default.
func AccessLock() error {
	err := caller("AccessLock")
	if err != nil {
		return err
	}

	if state.GetBool("disable") {
		return nil
	}

	if !state.GetBool("accessLock") {
		state.Set("accessLock", true)
	}

	if state.GetBool("isFirstAccess") {
		err := Access("/dev/null", "r")
		if err != nil {
			logger.Println("Failed to unveil /dev/null (r):", err)
		}
	}

	return unix.UnveilBlock()
}
