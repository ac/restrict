// SPDX-FileCopyrightText: 2023 Antonino Catinello
// SPDX-License-Identifier: BSD-3-Clause

// Package restrict is a library to secure your application. Targeting mainly OpenBSD and Linux (amd64) with musl-libc. On other operating systems the functions are no-ops to provide seamless compatibility besides Trust() and the Resolver() variants.
//
//   - Jail ability: Using Trust() and Resolver() enables your application to be independent from the systems typical rootCA bundle and allows the usage from system independent dns servers.
//   - Syscall allow-listing: Using Syscalls() gives you the ability to use the comfort of OpenBSD's pledge or seccomp on Linux in a pledge like variation.
//   - Restricting the filesystem view: Using Access()/AccessLock() lets you fine tune the accessibility of your filesystem with OpenBSD's unveil or Landlock on Linux in a unveil like mode.
package restrict

import (
	"os"
)

func abort(m string, err error) {
	logger.SetPrefix("restrict." + m + ": ")
	logger.Println(err)
	os.Exit(255)
}
