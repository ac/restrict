// SPDX-FileCopyrightText: 2024 Antonino Catinello
// SPDX-License-Identifier: BSD-3-Clause

// Make sure that your process has a mounted proc to see it self (process) as a requirement from libcap/psx 1.2.72 to manage tasks.³
//
//	³ https://git.kernel.org/pub/scm/libs/libcap/libcap.git/commit/?id=12e163ac21f11a8f8760305b9f60a6b7819aee7b
package restrict
