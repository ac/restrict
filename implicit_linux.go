// SPDX-FileCopyrightText: 2023 Antonino Catinello
// SPDX-License-Identifier: BSD-3-Clause

//go:build linux

package restrict

func implicitAccess(set string) []pathPerm {
	a := map[string][]pathPerm{
		"getpw": []pathPerm{
			pathPerm{"/etc/passwd", "r"},
			pathPerm{"/etc/shadow", "r"},
			pathPerm{"/etc/group", "r"},
			pathPerm{"/etc/nsswitch.conf", "r"},
			pathPerm{"/etc/rpc", "r"},
		},
		"dns": []pathPerm{
			pathPerm{"/etc/resolv.conf", "r"},
			pathPerm{"/etc/nsswitch.conf", "r"},
			pathPerm{"/etc/protocols", "r"},
			pathPerm{"/etc/services", "r"},
			pathPerm{"/etc/rpc", "r"},
			pathPerm{"/etc/hosts", "r"},
			pathPerm{"/etc/hostname", "r"},
		},
		"exec": []pathPerm{
			pathPerm{"/dev/null", "rw"},
			pathPerm{"/lib", "rx"},
			pathPerm{"/lib64", "rx"},
			pathPerm{"/usr/lib", "rx"},
			pathPerm{"/usr/lib64", "rx"},
			pathPerm{"/usr/libexec", "rx"},
			pathPerm{"/etc/ld.so.conf", "r"},
			pathPerm{"/etc/ld.so.conf.d", "r"},
			pathPerm{"/etc/ld.so.cache", "r"},
		},
		"stdio": []pathPerm{
			pathPerm{"/dev/null", "rw"},
			pathPerm{"/dev/zero", "r"},
			pathPerm{"/dev/urandom", "r"},
			pathPerm{"/dev/random", "r"},
			pathPerm{"/dev/full", "rw"},
			pathPerm{"/dev/log", "w"},
			pathPerm{"/run/systemd/journal/dev-log", "w"},
			pathPerm{"/etc/localtime", "r"},
			pathPerm{"/usr/share/locale", "r"},
			pathPerm{"/usr/share/zoneinfo", "r"},
			pathPerm{"/proc/self/cmdline", "r"},
		},
		"tmppath": []pathPerm{
			pathPerm{"/tmp", "crw"},
		},
		"tty": []pathPerm{
			pathPerm{"/dev/tty", "rw"},
			pathPerm{"/dev/console", "rw"},
			pathPerm{"/usr/lib/terminfo", "r"},
			pathPerm{"/usr/share/terminfo", "r"},
		},
		"ps": []pathPerm{
			pathPerm{"/proc", "r"},
		},
		"vminfo": []pathPerm{
			pathPerm{"/proc", "r"},
			pathPerm{"/sys", "r"},
		},
	}

	return a[set]
}
