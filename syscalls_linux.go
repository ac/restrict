// SPDX-FileCopyrightText: 2023 Antonino Catinello
// SPDX-License-Identifier: BSD-3-Clause

//go:build linux

package restrict

import (
	"errors"
	"strings"
	"syscall"

	"github.com/seccomp/libseccomp-golang"
)

var (
	defaultAction seccomp.ScmpAction = seccomp.ActKillProcess
	errorAction   seccomp.ScmpAction = seccomp.ActErrno.SetReturnCode(int16(syscall.ENOSYS))
)

func init() {
	var m string = "Syscalls"

	if !state.GetBool("disable") {
		v, err := seccomp.GetAPI()
		if err != nil {
			logger.Println("Seccomp error: ", err)
			Disable()
			return
		}

		if v < 6 {
			abort(m, errors.New("Seccomp API version needs to be 6 or greater."))
		}

		arch, err := seccomp.GetNativeArch()
		if err != nil {
			arch = seccomp.ArchNative
		}

		if arch != seccomp.ArchAMD64 {
			abort(m, errors.New("Your arch is not supported on linux yet."))
		}
	}
}

// Syscalls tries to behave similar to OpenBSD's pledge syscall.
//
// It forces that application in a syscall restricted operating mode by defining sets (a collection of syscalls, see promises on OpenBSD).
//
// You can call Syscalls() as often as needed, but you can only reduce the allowed sets from the previous call.
//
// The use of an unallowed syscall results in the default action to kill the process and notify about the incident in your kernel syslog. You can avoid this by applying the error set which will return ENOSYS to the calling function.
//
// SyscallsExec is a no-op on linux since Syscalls and Access are inheriting the restriction to it's childs.
func Syscalls(allow string) error {
	err := caller("Syscalls")
	if err != nil {
		return err
	}

	if state.GetBool("disable") {
		return nil
	}

	var filter *seccomp.ScmpFilter
	var special []string
	var hasError bool

	next := strings.Fields(allow)

	if contains(next, "error") {
		if state.GetBool("isFirstSyscalls") {
			hasError = true
		}

		filter, err = seccomp.NewFilter(errorAction)
		if err != nil {
			return err
		}
	} else {
		filter, err = seccomp.NewFilter(defaultAction)
		if err != nil {
			return err
		}
	}

	arch, err := seccomp.GetNativeArch()
	if err != nil {
		arch = seccomp.ArchNative
	}

	err = filter.AddArch(arch)
	if err != nil {
		return err
	}

	// make a local sets copy of global scope to use (ro)
	sets := state.GetStrings("syscalls")

	if state.GetBool("debug") {
		logger.Println(sets, next, state.GetBool("isFirstSyscalls"))
	}

	if subset(sets, next) || state.GetBool("isFirstSyscalls") {
		if !contains(next, "error") && hasError {
			hasError = false
		}

		state.Set("syscalls", next)
		// update local sets copy
		sets = next
	} else {
		// See error handling on calls with wider sets, be gentle and do nothing if error is set.
		if hasError {
			return nil
		} else {
			return errors.New("Syscalls can only be reduced after first call.")
		}
	}

	// is dns set?
	if contains(sets, "dns") {
		state.Set("dnsIsSet", true)
	} else {
		state.Set("dnsIsSet", false)
	}

	// is rpath set?
	if contains(sets, "rpath") {
		state.Set("rpathIsSet", true)
	} else {
		state.Set("rpathIsSet", false)
	}

	// Validate if a custom Resolver is used, then depend on 'dns'.
	// If Resolver was set and dns is missing, then return error.
	if state.GetBool("isFirstResolver") == false && state.GetBool("dnsIsSet") == false {
		return errors.New("restrict.Resolver depends on 'dns' as set.")
	}

	// Do the Access calls before applying the Syscalls filter.

	// Implicit Access to files depending on the given sets and if unveil and path sets are used.
	// This can be applied as often as needed, until AccessLock() was called from users code.
	if (contains(sets, "unveil") || contains(sets, "landlock")) && state.GetBool("isFirstSyscalls") {
		for _, s := range sets {
			if state.GetBool("debug") {
				logger.Println("Implicit Access() call: " + s)
			}

			for _, f := range implicitAccess(s) {
				state.Set("implicit", true)

				if exists(f.path) {
					err := Access(f.path, f.perm)
					if state.GetBool("debug") {
						logger.Print("Add implicit Access(" + f.path + ", " + f.perm + ")")
						if err != nil {
							logger.Println(err)
						}

					}
				}
			}
		}
	}

	// Workaround to block file access methods depending on choice of sets, since open() enables all on linux.
	if !contains(sets, "unveil") && !contains(sets, "landlock") && state.GetBool("isFirstSyscalls") {
		for _, s := range sets {
			for _, f := range implicitAccess(s) {
				state.Set("implicit", true)

				if exists(f.path) {
					err := Access(f.path, f.perm)
					if state.GetBool("debug") {
						logger.Print("Add implicit Access(" + f.path + ", " + f.perm + ")")
						if err != nil {
							logger.Println(err)
						}

					}
				}
			}
		}

		var perm string

		if contains(sets, "cpath") {
			perm = "c"
		}

		if contains(sets, "rpath") {
			perm = perm + "r"
		}

		if contains(sets, "wpath") {
			perm = perm + "w"
		}

		if contains(sets, "exec") {
			perm = perm + "x"
		}

		if len(perm) > 0 {
			// "/" aka root always exists!
			err := Access("/", perm)
			if err != nil {
				return err
			}

			if state.GetBool("debug") {
				logger.Print("Allow filesystem to Access(\"/\", " + perm + ")")
			}
		}

		if len(perm) > 0 || state.GetBool("implicit") {
			err = AccessLock()
			if err != nil {
				return err
			}

			if state.GetBool("debug") {
				logger.Print("Lock filesystem view")
			}

		}
	}

	if state.GetBool("implicit") {
		special = append(special, "openat")
		special = append(special, "readlinkat")
	}

	if state.GetBool("isFirstSyscalls") {
		state.Set("isFirstSyscalls", false)
	}

	if state.GetBool("debug") {
		logger.Println("Special:", special)
	}

	err = setFilter(filter, append(setsToSyscalls(sets), special...))
	if err != nil {
		return err
	}

	filter.Release()

	return nil
}

// SyscallsExec is a no-op on linux.
//
// Deprecated: Landlock¹ and seccomp² calls are inherited on Syscalls() with linux.
//
//	¹ https://www.kernel.org/doc/html/latest/userspace-api/landlock.html#inheritance
//	² https://www.kernel.org/doc/html/latest/userspace-api/seccomp_filter.html#usage
func SyscallsExec(allow string) error {
	return nil
}

func setsToSyscalls(sets []string) []string {
	var sc []string

	for _, s := range sets {
		v, ok := syscallsMap(s)
		if ok {
			sc = append(sc, v...)
		}
	}

	sc = append(sc, baseSyscalls...)

	dedupe := make(map[string]bool)
	var r []string
	for _, s := range sc {
		if _, ok := dedupe[s]; !ok {
			dedupe[s] = true
			r = append(r, s)
		}
	}

	return r
}

// is next a subset of prev?
func subset(prev, next []string) bool {
	var counter int

	lp := len(prev)
	ln := len(next)

	if ln == 0 {
		return true
	}

	if lp < ln {
		return false
	}

	for d, n := range next {
		if state.GetBool("debug") {
			logger.Println("Compare: ", n)
		}

		for _, i := range prev {
			if state.GetBool("debug") {
				logger.Println(i, " == ", n)
			}

			// found skip to next
			if i == n {
				counter++
				break
			}
		}

		// not found
		if d == ln-1 && counter != ln {
			return false
		}
	}

	return true
}

func syscallsFilter(promises string) string {
	var pp string

	for _, p := range strings.Fields(promises) {
		v, ok := syscallsMap(p)
		if ok {
			if len(v) > 0 && len(pp) > 0 {
				pp = pp + " " + strings.Join(v, " ")
			} else {
				pp = strings.Join(v, " ")
			}
		} else {
			if len(pp) > 0 {
				pp = pp + " " + p
			} else {
				pp = p
			}
		}
	}

	return pp
}

func setFilter(filter *seccomp.ScmpFilter, allow []string) error {
	for _, name := range allow {
		id, err := seccomp.GetSyscallFromName(name)
		if err != nil {
			return err
		}

		err = filter.AddRule(id, seccomp.ActAllow)
		if err != nil {
			return err
		}
	}

	if filter.IsValid() {
		return filter.Load()
	} else {
		return errors.New("Seccomp filter is invalid.")
	}
}

func syscallsMap(set string) ([]string, bool) {
	s := map[string][]string{
		"stdio":   stdioSyscalls,
		"rpath":   rpathSyscalls,
		"wpath":   wpathSyscalls,
		"cpath":   cpathSyscalls,
		"dpath":   dpathSyscalls,
		"tmppath": tmppathSyscalls,
		"inet":    inetSyscalls,
		//		"mcast":        mcastSyscalls,
		"mcast":     inetSyscalls, // use inet instead
		"fattr":     fattrSyscalls,
		"chown":     chownSyscalls,
		"flock":     flockSyscalls,
		"unix":      unixSyscalls,
		"dns":       dnsSyscalls,
		"getpw":     getpwSyscalls,
		"sendfd":    sendfdSyscalls,
		"recvfd":    recvfdSyscalls,
		"tape":      tapeSyscalls,
		"tty":       ttySyscalls,
		"proc":      procSyscalls,
		"exec":      execSyscalls,
		"prot_exec": prot_execSyscalls, // openbsd only
		"settime":   settimeSyscalls,
		"ps":        psSyscalls,
		"vminfo":    vminfoSyscalls,
		"id":        idSyscalls,
		"pf":        pfSyscalls, // openbsd only
		//		"route":        routeSyscalls,
		"route": inetSyscalls, // rtnetlink via socket, so use inet
		//		"wroute":       wrouteSyscalls,
		"wroute": inetSyscalls, // rtnetlink via socket, so use inet
		"audio":  audioSyscalls,
		"video":  videoSyscalls,
		"bpf":    bpfSyscalls,
		"unveil": unveilSyscalls, // openbsd only / see landlock
		"error":  errorSyscalls,
		// linux additions
		"landlock":     unveilSyscalls,       // alias for unveil
		"capabilities": capabilitiesSyscalls, // linux only / no-op on openbsd
		"namespaces":   namespacesSyscalls,   // linux only / no-op on openbsd
	}

	v, ok := s[set]
	return v, ok
}
