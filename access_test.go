// SPDX-FileCopyrightText: 2023 Antonino Catinello
// SPDX-License-Identifier: BSD-3-Clause

//go:build linux || openbsd

package restrict

import (
	"fmt"
	"os"
	"testing"
)

func TestAccess(t *testing.T) {
	err := Access("/etc/passwd", "r")
	if err != nil {
		panic(err)
	}

	err = Access("/dev/null/foo", "crwx")
	if err != nil {
		fmt.Println("/dev/null/foo → fails!")
	}

	//err = Access("/root", "rwx")
	err = Access("/home", "rwx")
	if err != nil {
		panic(err)
	}

	err = AccessLock()
	if err != nil {
		panic(err)
	}

	_, err = os.ReadFile("/etc/resolv.conf")
	fmt.Println("/etc/resolv.conf → ", err)

	_, err = os.ReadFile("/etc/passwd")
	fmt.Println("/etc/passwd → ", err)
}

func TestDisable(t *testing.T) {
	fmt.Println(Disable())
	return
}
