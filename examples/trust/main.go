package main

import (
	"io"
	"log"
	"net/http"

	"catinello.eu/lecot"
	"catinello.eu/restrict"
)

func main() {
	//restrict.Debug()

	if err := restrict.Syscalls("stdio dns inet"); err != nil {
		log.Fatal(err)
	}

	// adds only LetsEncrypt Chain of Trust
	restrict.Trust(lecot.Pool())

	client := &http.Client{}
	req, err := http.NewRequest("GET", "https://ip.ac-x.eu", nil)
	if err != nil {
		log.Fatal(err)
	}

	resp, err := client.Do(req)
	if err != nil {
		log.Fatal(err)
	}

	defer resp.Body.Close()

	bodyText, err := io.ReadAll(resp.Body)
	if err != nil {
		log.Fatal(err)
	}

	log.Printf("%s\n", bodyText)

	// example.com fails verification / needs DigiCert CA
	req, err = http.NewRequest("GET", "https://example.com", nil)
	if err != nil {
		log.Fatal(err)
	}

	resp, err = client.Do(req)
	if err != nil {
		log.Fatal(err)
	}

	defer resp.Body.Close()

	bodyText, err = io.ReadAll(resp.Body)
	if err != nil {
		log.Fatal(err)
	}

	log.Printf("%s\n", bodyText)
}
