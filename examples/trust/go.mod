module restrict/cmd/trust

go 1.21.1


require (
	catinello.eu/lecot v0.0.0-20240321105049-8a004ccdbf23
	catinello.eu/restrict v0.0.0-20240926153032-ef3a581dcc9a
)

require (
	github.com/seccomp/libseccomp-golang v0.10.0 // indirect
	golang.org/x/sys v0.25.0 // indirect
	kernel.org/pub/linux/libs/security/libcap/psx v1.2.70 // indirect
)
