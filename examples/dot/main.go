package main

import (
	"log"
	"net"

	"catinello.eu/restrict"
)

func main() {
	//restrict.Debug()

	// you need rpath to access CA's to verify
	if err := restrict.Syscalls("stdio rpath dns inet"); err != nil {
		log.Fatal(err)
	}

	// needs to disable TLS verify, since digitalcourage's IP is not included in cert
	//restrict.ResolverTLSVerify = false
	//if err := restrict.ResolverX("dns3.digitalcourage.de", "853", true); err != nil {

	// works as their IP addresses are valid
	//if err := restrict.ResolverTLS("2620:fe::fe"); err != nil {
	if err := restrict.ResolverTLS("dns.sb"); err != nil {
		log.Fatal(err)
	}

	addrs, err := net.LookupHost("example.com")
	if err != nil {
		log.Fatal(err)
	}

	log.Println(addrs)
}
