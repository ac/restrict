package main

import (
	"io"
	"log"
	"net/http"

	"catinello.eu/restrict"
)

func main() {
	if err := restrict.Syscalls("stdio rpath dns inet unveil"); err != nil {
		log.Fatal(err)
	}

	if err := restrict.AccessCerts(); err != nil {
		log.Fatal(err)
	}

	if err := restrict.AccessLock(); err != nil {
		log.Fatal(err)
	}

	client := &http.Client{}
	req, err := http.NewRequest("GET", "https://ip.ac-x.eu", nil)
	if err != nil {
		log.Fatal(err)
	}

	resp, err := client.Do(req)
	if err != nil {
		log.Fatal(err)
	}

	defer resp.Body.Close()

	bodyText, err := io.ReadAll(resp.Body)
	if err != nil {
		log.Fatal(err)
	}

	log.Printf("%s", bodyText)
}
