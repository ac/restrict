package main

import (
	"log"
	"net"

	"catinello.eu/restrict"
)

func main() {
	if err := restrict.Syscalls("stdio dns inet"); err != nil {
		log.Fatal(err)
	}

	if err := restrict.Resolver("2620:fe::fe"); err != nil {
		log.Fatal(err)
	}

	addrs, err := net.LookupHost("example.com")
	if err != nil {
		log.Fatal(err)
	}

	log.Println(addrs)
}
