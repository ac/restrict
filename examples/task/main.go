//go:build linux

package main

import (
	"errors"
	"fmt"
	"io/fs"
	"log"
	"os"
	"syscall"

	"catinello.eu/restrict"
)

func main() {
	//	restrict.Debug()

	if err := restrict.Syscalls("stdio rpath unveil"); err != nil {
		log.Fatal(err)
	}

	// implicit access to /proc/self/task
	cmd := fmt.Sprintf("/proc/self/task/%d/cmdline", syscall.Gettid())
	root := fmt.Sprintf("/proc/self/task/%d/root", syscall.Gettid())

	// explicit acccess to CWD
	cwd := fmt.Sprintf("/proc/self/task/%d/cwd", syscall.Gettid())

	wd, err := os.Getwd()
	if err != nil {
		log.Fatal(err)
	}

	// allows cwd symlink target path
	if err := restrict.Access(wd, "r"); err != nil {
		log.Fatal(err)
	}

	if err := restrict.AccessLock(); err != nil {
		log.Fatal(err)
	}

	// success to access values in /proc/self/task
	data, err := os.ReadFile(cmd)
	if err != nil {
		log.Fatal(err)
	}
	log.Println(string(data))

	// fails to follow root
	dir, err := os.ReadDir(root)
	if err != nil {
		if errors.Is(err, fs.ErrPermission) {
			log.Println("Permission denied.")
		} else {
			log.Fatal(err)
		}
	} else {
		log.Println(dir)
	}

	// success to follow CWD because it was explicitly allowed above
	wddir, err := os.ReadDir(cwd)
	if err != nil {
		log.Fatal(err)
	}
	log.Println(wddir)
}
