package main

import (
	"log"
	"os"
	"time"

	"catinello.eu/restrict"
)

func main() {
	if err := restrict.Syscalls("stdio cpath rpath wpath landlock fattr"); err != nil {
		log.Fatal(err)
	}

	path := "/tmp"
	if err := restrict.Access(path, "crw"); err != nil {
		log.Fatal(err)
	}

	if err := restrict.AccessLock(); err != nil {
		log.Fatal(err)
	}

	fileName := path + "/touch.test"
	_, err := os.Stat(fileName)
	if os.IsNotExist(err) {
		file, err := os.Create(fileName)
		if err != nil {
			log.Fatal(err)
		}
		defer file.Close()
	} else {
		currentTime := time.Now().Local()
		err = os.Chtimes(fileName, currentTime, currentTime)
		if err != nil {
			log.Fatal(err)
		}
	}

}
