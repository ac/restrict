package main

import (
	"log"
	"os"

	"catinello.eu/restrict"
)

func main() {
	if err := restrict.Syscalls("stdio tmppath"); err != nil {
		log.Fatal(err)
	}

	f, err := os.CreateTemp("", "example")
	// see /tmp/example*
	//	defer os.Remove(f.Name())

	if err != nil {
		log.Fatal(err)
	}

	if _, err := f.Write([]byte("test")); err != nil {
		log.Fatal(err)
	}

	if err := f.Close(); err != nil {
		log.Fatal(err)
	}
}
