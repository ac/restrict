package main

import (
	"log"
	"os"

	"catinello.eu/restrict"
)

func main() {
	var file string = "/etc/hosts"

	//if err := restrict.Syscalls("stdio unveil error"); err != nil {
	if err := restrict.Syscalls("stdio rpath unveil"); err != nil {
		log.Fatal(err)
	}

	if err := restrict.Access(file, "r"); err != nil {
		log.Fatal(err)
	}

	if err := restrict.AccessLock(); err != nil {
		log.Fatal(err)
	}

	data, err := os.ReadFile(file)
	if err != nil {
		log.Fatal(err)
	}

	log.Println(string(data))
}
