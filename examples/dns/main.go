package main

import (
	"log"
	"net"

	"catinello.eu/restrict"
)

func main() {
	if err := restrict.Syscalls("stdio dns"); err != nil {
		log.Fatal(err)
	}

	addrs, err := net.LookupHost("example.com")
	if err != nil {
		log.Fatal(err)
	}

	log.Println(addrs)
}
