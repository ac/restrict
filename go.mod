module catinello.eu/restrict

go 1.23.1

require (
	catinello.eu/inet v0.0.0-20241127115856-0e99a8ad6f71
	catinello.eu/store v0.0.0-20250213134558-efb16c19f7d1
	github.com/seccomp/libseccomp-golang v0.10.0
	golang.org/x/sys v0.30.0
	kernel.org/pub/linux/libs/security/libcap/psx v1.2.73
)
