// SPDX-FileCopyrightText: 2024 Antonino Catinello
// SPDX-License-Identifier: BSD-3-Clause

//go:build linux || openbsd

package restrict // "catinello.eu/restrict"

import (
	"errors"
	"runtime"
)

// AccessCerts is a convenience function. It allows the typical paths to your
// operating systems CA and certificate locations to be read.
//
// It represents an alias to Access("/etc/ssl/cert.pem", "r") on OpenBSD to allow reads.
// See below for files used on Linux/others. You must use AccessLock() to actually
// lock the filesystem view on Linux. See Access() for details on Linux documentation.
//
//	OpenBSD: "/etc/ssl/cert.pem"
//	Linux: "/etc/ssl/certs", "/etc/ca-certificates", "/etc/ssl/cert.pem", "/etc/pki"
//	Others: no-op
func AccessCerts() error {
	err := caller("AccessCerts")
	if err != nil {
		return err
	}

	if state.GetBool("disable") {
		return nil
	}

	if !state.GetBool("accessCerts") {
		state.Set("accessCerts", true)
	}

	// Was Syscalls already called and rpath is not set, then return error.
	if state.GetBool("isFirstSyscalls") == false && state.GetBool("rpathIsSet") == false {
		return errors.New("restrict.AccessCerts() needs 'rpath' as Syscalls() set if Access() is restricted.")
	}

	switch runtime.GOOS {
	case "linux":
		paths := []string{"/etc/ssl/certs", "/etc/ca-certificates", "/etc/ssl/cert.pem", "/etc/pki"}

		for _, p := range paths {
			if exists(p) {
				if err := Access(p, "r"); err != nil {
					return err
				}
			}
		}
	case "openbsd":
		if err := Access("/etc/ssl/cert.pem", "r"); err != nil {
			return err
		}
	}

	return nil
}
