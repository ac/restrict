// SPDX-FileCopyrightText: 2023 Antonino Catinello
// SPDX-License-Identifier: BSD-3-Clause

//go:build linux || openbsd

package restrict

import (
	"fmt"
	"testing"
)

func TestSyscalls(t *testing.T) {
	// test all
	err := Syscalls("stdio error proc rpath wpath cpath dpath tmppath inet mcast fattr chown flock dns getpw sendfd recvfd tape tty exec prot_exec settime ps vminfo id pf route wroute audio video bpf unveil capabilities landlock")
	if err != nil {
		panic(err)
	}

	err = Syscalls("stdio")
	if err != nil {
		panic(err)
	}

	err = Syscalls("stdio proc")
	if err != nil {
		fmt.Println("Fail adding after reduce:", err)
	}

}

func TestSubset(t *testing.T) {
	a := []string{"foo", "bar"}
	b := []string{"foo"}
	c := []string{"foo", "bar", "abc"}
	d := []string{"foo", "abc"}

	println(subset(a, b)) // true
	println(subset(a, c)) // false
	println(subset(a, d)) // false
	println(subset(a, a)) // true
}

func TestFilter(t *testing.T) {
	println(syscallsFilter("stdio unveil capabilities landlock"))
}
