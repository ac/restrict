// SPDX-FileCopyrightText: 2022 Antonino Catinello
// SPDX-License-Identifier: BSD-3-Clause

package restrict // import "catinello.eu/restrict"

import (
	"errors"
	"fmt"
	"log"
	"net"
	"os"
	"runtime"
	"strings"
	"time"

	"catinello.eu/store"
)

type kernel struct {
	major uint16
	minor uint16
	patch uint16
}

type attribute uint64

type permissions struct {
	path  string
	date  time.Time
	attr  attribute
	flags string
}

type pathPerm struct {
	path string
	perm string
}

var (
	state = store.New()

	// Logger defaults to Stderr and adds a prefix of "restrict: "
	DefaultLogger = log.New(os.Stderr, "restrict: ", 0)
	logger        = DefaultLogger
)

// initialize state
func init() {
	state.Set("debug", false)
	state.Set("disable", false)
	state.Set("expert", false)

	state.Set("isFirstAccess", true)
	state.Set("isFirstSyscalls", true)
	state.Set("isFirstTrust", true)
	state.Set("isFirstResolver", true)

	state.Set("access", []permissions{})
	state.Set("accessCerts", false)
	state.Set("accessLock", false)

	state.Set("dns", "")
	state.Set("dnsIP", net.IP{})
	state.Set("dnsProto", UDP)

	state.Set("syscalls", []string{})
	state.Set("implicit", false)
	state.Set("dnsIsSet", false)
	state.Set("rpathIsSet", false)
}

// Debug toggles debug messages to Logger.
func Debug() {
	d := state.GetBool("debug")

	state.Set("debug", !d)
}

// Disable gives you the ability to let all restrict function calls be nil returned, except the Trust function that returns an empty *tls.Config.
//
// Beware! It has to be called before the first restrict function call of Syscalls/Access/Trust/Resolver in the application and all protections are off.
// A notice will be printed to Logger:
//
// "restrict: All subsequent restrict calls are disabled on your demand!"
func Disable() error {
	err := caller("Disable")
	if err != nil {
		return err
	}

	if state.GetBool("isFirstSyscalls") && state.GetBool("isFirstAccess") && state.GetBool("isFirstResolver") && state.GetBool("isFirstTrust") {
		state.Set("disable", true)
		logger.Println("All subsequent restrict calls are disabled on your demand!")

		return nil
	}

	return errors.New("Your call of restrict.Disable() was too late.")
}

// Expert enables a mode to use restrict outside of main().
//
// Beware! Do not use this unless you really know what you are doing.
func Expert() {
	err := caller("Expert")
	if err != nil {
		logger.Println(err)
	}

	state.Set("expert", true)
}

func caller(called string) error {
	if state.GetBool("expert") {
		return nil
	}

	pc, _, _, ok := runtime.Caller(2)
	if !ok {
		return errors.New("Could not determine caller.")
	}

	f := runtime.FuncForPC(pc).Name()

	if state.GetBool("debug") {
		logger.Println("Caller:", f)
	}

	if f != "main.main" && !strings.HasPrefix(f, "catinello.eu/restrict.") {
		return errors.New("Must not call restrict." + called + " from outside main(). (see restrict.Expert())")
	}

	return nil
}

// State prints the actual state of all available restrictions to DefaultLogger().
//
//	restrict: Kernel: 7.4
//	restrict: Expert: false
//	restrict: Access: true
//	restrict: · Implicit: false
//	restrict: · Lock: true
//	restrict: Syscalls: true
//	restrict: Resolver: true
//	restrict: · Target: dns.sb:853 [2a09::] (TLS)
//	restrict: Trust: false
//	restrict: · Local: false
//	restrict: Sets: [stdio rpath dns inet]
//	restrict: Paths:
//	restrict: → /home (r)
func State() {
	k, _ := state.Get("kernel")
	logger.Println("Kernel:", k.(kernel))
	logger.Println("Expert:", state.GetBool("expert"))

	if runtime.GOOS == "linux" {
		logger.Println("ABI:", state.GetInt("abi"))
	}

	logger.Println("Access:", !state.GetBool("isFirstAccess"))
	logger.Println("· Implicit:", state.GetBool("implicit"))
	logger.Println("· Lock:", state.GetBool("accessLock"))
	logger.Println("Syscalls:", !state.GetBool("isFirstSyscalls"))
	logger.Println("Resolver:", !state.GetBool("isFirstResolver"))

	if !state.GetBool("isFirstResolver") {
		ip, _ := state.Get("dnsIP")
		proto, _ := state.Get("dnsProto")
		logger.Printf("· Target: %v [%v] (%v)\n", state.GetString("dns"), ip.(net.IP), proto)
	}

	logger.Println("Trust:", !state.GetBool("isFirstTrust"))
	logger.Println("· Local:", state.GetBool("accessCerts"))
	logger.Println("Sets:", state.GetStrings("syscalls"))
	logger.Println("Paths:")
	a, _ := state.Get("access")
	for _, p := range a.([]permissions) {
		logger.Printf("→ %v (%v)\n", p.path, p.flags)
	}
}

func (k kernel) String() string {
	if runtime.GOOS == "linux" {
		return fmt.Sprintf("%v.%v.%v", k.major, k.minor, k.patch)
	}

	if runtime.GOOS == "openbsd" {
		return fmt.Sprintf("%v.%v", k.major, k.minor)
	}

	return ""
}
