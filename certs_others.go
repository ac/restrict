// SPDX-FileCopyrightText: 2024 Antonino Catinello
// SPDX-License-Identifier: BSD-3-Clause

//go:build !linux && !openbsd

package restrict // "catinello.eu/restrict"

// AccessCerts is a no-op that returns nil on this unsupported OS.
func AccessCerts() error {
	return nil
}
