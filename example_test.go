// SPDX-FileCopyrightText: 2025 Antonino Catinello
// SPDX-License-Identifier: BSD-3-Clause

package restrict_test

import (
	"log"
	"net"
	"os"

	"catinello.eu/restrict"
)

func ExampleAccess() {
	var file string = "/etc/hosts"

	// Allow read access to file.
	if err := restrict.Access(file, "r"); err != nil {
		log.Fatal(err)
	}

	// Lock the defined access above.
	if err := restrict.AccessLock(); err != nil {
		log.Fatal(err)
	}

	// Only file is available, no other files.
	data, err := os.ReadFile(file)
	if err != nil {
		log.Fatal(err)
	}

	log.Println(string(data))
}

func ExampleSyscalls() {
	// Allow only stdio and dns use.
	if err := restrict.Syscalls("stdio dns"); err != nil {
		log.Fatal(err)
	}

	addrs, err := net.LookupHost("example.com")
	if err != nil {
		log.Fatal(err)
	}

	log.Println(addrs)
}
