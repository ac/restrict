// SPDX-FileCopyrightText: 2023 Antonino Catinello
// SPDX-License-Identifier: BSD-3-Clause

//go:build linux && amd64

// Sets aka promises available on linux:
//   - stdio
//   - rpath
//   - wpath
//   - cpath
//   - dpath
//   - tmppath
//   - inet
//   - mcast (stub)
//   - fattr
//   - chown
//   - flock
//   - unix
//   - dns
//   - getpw
//   - sendfd
//   - recvfd
//   - tape (stub)
//   - tty
//   - proc
//   - exec
//   - prot_exec (openbsd only)
//   - settime
//   - ps
//   - vminfo
//   - id
//   - pf (openbsd only)
//   - route (stub)
//   - wroute (stub)
//   - audio
//   - video
//   - bpf
//   - unveil (alias landlock)
//   - error
//
// See https://man.openbsd.org/pledge.2 for more info.
//
// Linux only:
//   - capabilities
//   - namespaces
package restrict

var (
	// mandatory
	baseSyscalls []string = []string{"clone",
		"syslog",
		"seccomp",
		"restart_syscall",
		"tkill",
		"tgkill",
		"clone3",
		"exit",
		"exit_group",
		"fanotify_init",
		"fanotify_mark",
		"getcpu",
		"gettid",
		"ipc",
		"msgctl",
		"msgget",
		"msgrcv",
		"msgsnd",
		"pause",
		"rseq",
		"sched_getaffinity",
		"getrlimit",
		"prlimit64",
	}

	// openbsd matching promises
	stdioSyscalls []string = []string{"close",
		"getrusage",
		"rt_sigprocmask",
		"rt_sigreturn",
		"rt_sigaction",
		"rt_sigpending",
		"rt_sigqueueinfo",
		"rt_sigsuspend",
		"rt_sigtimedwait",
		"rt_tgsigqueueinfo",
		"epoll_create",
		"epoll_create1",
		"epoll_ctl",
		"epoll_pwait",
		"epoll_pwait2",
		"epoll_wait",
		"eventfd",
		"eventfd2",
		"futex",
		"get_robust_list",
		"set_robust_list",
		"sched_yield",
		"close_range",
		"dup",
		"dup2",
		"dup3",
		"fchdir",
		"fstat",
		"fsync",
		"fdatasync",
		"ftruncate",
		"getdents",
		"getdents64",
		"getegid",
		"getrandom",
		"geteuid",
		"getgid",
		"getgroups",
		"getitimer",
		"getpgid",
		"getpgrp",
		"getpid",
		"getppid",
		"getresgid",
		"getresuid",
		"getsid",
		"wait4",
		"waitid",
		"gettimeofday",
		"getuid",
		"lseek",
		"madvise",
		"brk",
		"arch_prctl",
		"prctl",
		"uname",
		"set_tid_address",
		"clock_getres",
		"clock_gettime",
		"clock_nanosleep",
		"mmap",
		"mremap",
		"mprotect",
		"msync",
		"munmap",
		"membarrier",
		"nanosleep",
		"pipe",
		"pipe2",
		"read",
		"readv",
		"pread64",
		"recv",
		"poll",
		"ppoll",
		"recvfrom",
		"recvmsg",
		"preadv",
		"write",
		"writev",
		"pwrite64",
		"pwritev",
		"pwritev2",
		"select",
		"pselect6",
		"send",
		"sendto",
		"setitimer",
		"shutdown",
		"sigaction",
		"sigaltstack",
		"sigprocmask",
		"sigreturn",
		"sigsuspend",
		"umask",
		"socketpair",
		"ioctl",
		"fcntl",
		"fallocate",
		"newfstatat",
		"sendmsg",
		"sendmmsg",
		"open",
		"tee",
		"splice",
		"vmsplice",
		"io_uring_setup",
		"timerfd_create",
		"timerfd_settime",
		"timerfd_gettime",
	}

	rpathSyscalls     []string = []string{"chdir", "getcwd", "openat", "stat", "fstat", "lstat", "fstatat64", "access", "faccessat", "faccessat2", "readlink", "readlinkat", "statfs", "fstatfs", "getxattr", "fgetxattr", "flistxattr", "listxattr", "chmod", "fchmod", "fchmodat", "fadvise64", "statx"}
	wpathSyscalls     []string = []string{"getcwd", "openat", "stat", "fstat", "lstat", "fstatat64", "access", "faccessat", "faccessat2", "readlink", "readlinkat", "chmod", "fchmod", "fchmodat", "fremovexattr", "fsetxattr", "removexattr", "fchownat", "fallocate", "io_uring_enter", "io_uring_register"}
	cpathSyscalls     []string = []string{"openat", "rename", "renameat", "renameat2", "link", "linkat", "symlink", "symlinkat", "unlink", "rmdir", "unlinkat", "mkdir", "mkdirat", "creat"}
	dpathSyscalls     []string = []string{"mknod", "mknodat"}
	tmppathSyscalls   []string = []string{"chmod", "unlink", "unlinkat", "lstat", "fstat", "chown", "open", "fallocate"}
	inetSyscalls      []string = []string{"socket", "listen", "bind", "connect", "accept", "accept4", "getpeername", "getsockname", "setsockopt", "getsockopt", "sendto"}
	mcastSyscalls     []string = []string{"setsockopt"}
	fattrSyscalls     []string = []string{"chmod", "fchmod", "fchmodat", "utime", "utimes", "futimesat", "utimensat", "chown", "fchown", "lchown", "fchownat"}
	chownSyscalls     []string = []string{"chown"}
	flockSyscalls     []string = []string{"flock", "fcntl", "open"}
	unixSyscalls      []string = []string{"socket", "listen", "bind", "connect", "accept", "accept4", "getpeername", "getsockname", "setsockopt", "getsockopt"}
	dnsSyscalls       []string = []string{"socket", "sendto", "recvfrom", "connect", "setsockopt", "bind", "getpeername", "getsockname", "open"}
	getpwSyscalls     []string = []string{}
	sendfdSyscalls    []string = []string{"sendmsg", "sendmmsg", "sendfile", "sendfile64"}
	recvfdSyscalls    []string = []string{"recvmsg"}
	tapeSyscalls      []string = []string{}
	ttySyscalls       []string = []string{"ioctl"}
	procSyscalls      []string = []string{"fork", "vfork", "kill", "getpriority", "setpriority", "prlimit64", "setrlimit", "setpgid", "setsid", "sched_getscheduler", "sched_setscheduler", "sched_get_priority_min", "sched_get_priority_max", "sched_getparam", "sched_setparam"}
	execSyscalls      []string = []string{"execve", "execveat", "memfd_create", "pidfd_open", "pidfd_send_signal"}
	prot_execSyscalls []string = []string{}
	settimeSyscalls   []string = []string{"settimeofday", "clock_adjtime", "adjtimex", "clock_settime"}
	psSyscalls        []string = []string{"pidfd_open", "pidfd_send_signal"}
	vminfoSyscalls    []string = []string{"sysinfo", "sysfs"}
	idSyscalls        []string = []string{"setuid", "setreuid", "setresuid", "setgid", "setregid", "setresgid", "setgroups", "prlimit64", "setrlimit", "getpriority", "setpriority", "setfsuid", "setfsgid"}
	pfSyscalls        []string = []string{}
	routeSyscalls     []string = []string{}
	wrouteSyscalls    []string = []string{}
	audioSyscalls     []string = []string{"ioctl", "memfd_create"}
	videoSyscalls     []string = []string{"ioctl"}
	bpfSyscalls       []string = []string{"bpf"}
	unveilSyscalls    []string = []string{"landlock_restrict_self", "landlock_create_ruleset", "landlock_add_rule"}
	errorSyscalls     []string = []string{}

	// linux only / openbsd no-ops
	capabilitiesSyscalls []string = []string{"capget", "capset"}
	namespacesSyscalls   []string = []string{"setns", "unshare"}
	// module, mount, hostname, inotify, aio (io_*), kcmp, keyctl, memfd_*, mlock*, mq_*, nice, pkey_*, ptrace, reboot, sched_*, sem*, setns, shm*, swap, sync, syncfs, timer_*
)
