// SPDX-FileCopyrightText: 2022 Antonino Catinello
// SPDX-License-Identifier: BSD-3-Clause

//go:build !openbsd && !linux

package restrict

// Syscalls is a no-op on this unsupported OS.
func Syscalls(allowed string) error {
	return nil
}

// SyscallsExec is a no-op this on unsupported OS.
func SyscallsExec(allowed string) error {
	return nil
}
