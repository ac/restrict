// SPDX-FileCopyrightText: 2022 Antonino Catinello
// SPDX-License-Identifier: BSD-3-Clause

package restrict

import (
	"crypto/tls"
	"crypto/x509"
	"net/http"
)

// Trust sets the RootCA to your given pool and sets default http transport TLS config and returns a pointer to that tls.Config object for you to use it.
//
// Beware! A previous Disable() call will return a barely initialized TLS config object. A call from outside main() without Expert() mode enabled, will print an error message to the DefaultLogger but still return a valid pointer to that tls.Config object.
func Trust(pool *x509.CertPool) *tls.Config {
	err := caller("Trust")
	if err != nil {
		logger.Println(err)
	}

	var cfg *tls.Config

	if !state.GetBool("disable") {
		cfg = &tls.Config{
			RootCAs:                  pool,
			MinVersion:               tls.VersionTLS13,
			PreferServerCipherSuites: true,
		}

		http.DefaultTransport.(*http.Transport).TLSClientConfig = cfg
	}

	if state.GetBool("isFirstTrust") {
		state.Set("isFirstTrust", false)
	}

	return cfg
}
