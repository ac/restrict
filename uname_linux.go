// SPDX-FileCopyrightText: 2021 Antonino Catinello
// SPDX-License-Identifier: BSD-3-Clause

//go:build linux

package restrict

import (
	"bytes"
	"strconv"
	"syscall"

	"golang.org/x/sys/unix"
)

type utsname struct {
	Sysname    []byte
	Nodename   []byte
	Release    []byte
	Version    []byte
	Machine    []byte
	Domainname []byte
}

// Uname returns a pointer on a filled datastrutre of Utsname.
func uname() (*utsname, error) {
	u := syscall.Utsname{}
	if err := syscall.Uname(&u); err != nil {
		return nil, err
	}

	convert := func(f [65]int8) []byte {
		out := make([]byte, 0, 64)
		for _, v := range f[:] {
			if v == 0 {
				break
			}
			out = append(out, uint8(v))
		}
		return out
	}

	uname := utsname{
		Sysname:    convert(u.Sysname),
		Nodename:   convert(u.Nodename),
		Release:    convert(u.Release),
		Version:    convert(u.Version),
		Machine:    convert(u.Machine),
		Domainname: convert(u.Domainname),
	}

	return &uname, nil
}

// Kernel returns the release version in major, minor, patch (== 0) notation.
// We only check major and minor in system requirements and this avoids suffix variations.
func (u *utsname) Kernel() (uint16, uint16, uint16, error) {
	k := bytes.Split(u.Release, []byte("-"))[0]
	r := bytes.Split(k, []byte("."))[0:3]

	major, err := strconv.Atoi(unix.ByteSliceToString(r[0]))
	if err != nil {
		return 0, 0, 0, err
	}

	minor, err := strconv.Atoi(unix.ByteSliceToString(r[1]))
	if err != nil {
		return 0, 0, 0, err
	}

	patch, err := strconv.Atoi(unix.ByteSliceToString(r[2]))
	if err != nil {
		var num string
		var stop bool

		for _, v := range unix.ByteSliceToString(r[2]) {
			if stop {
				break
			}

			switch v {
			case '0', '1', '2', '3', '4', '5', '6', '7', '8', '9':
				num = num + string(v)
			default:
				stop = true
			}
		}

		if len(num) == 0 {
			return 0, 0, 0, err
		}

		patch, err = strconv.Atoi(num)
		if err != nil {
			return 0, 0, 0, err
		}
	}

	return uint16(major), uint16(minor), uint16(patch), nil
}
