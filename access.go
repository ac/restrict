// SPDX-FileCopyrightText: 2022 Antonino Catinello
// SPDX-License-Identifier: BSD-3-Clause

//go:build !linux && !openbsd

package restrict

// Access is a no-op that returns nil on this unsupported OS.
func Access(path string, flags string) error {
	return nil
}

// AccessLock is a no-op that returns nil on this unsupported OS.
func AccessLock() error {
	return nil
}
