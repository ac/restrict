// SPDX-FileCopyrightText: 2022 Antonino Catinello
// SPDX-License-Identifier: BSD-3-Clause

//go:build linux

// We are only supporting the Landlock ABI version (3, 4 is not implmented yet) and seccomp API version (6+).
// Kernels before version 6.x (formerly 5.13) and without Landlock¹ as LSM and seccomp² are not supported and will print warnings via DefaultLogger or result in an immediate abort on errors (code 255).
//
// Linux support is AMD64 only and musl-libc mainly. Happens to work with glibc as well. Glibc or other libc implementations are not tested or used in production by myself.
//
// ARM64 and/or RISCV64 support may come later at some point.
//
//	¹ https://www.kernel.org/doc/html/latest/userspace-api/landlock.html
//	² https://www.kernel.org/doc/html/latest/userspace-api/seccomp_filter.html
package restrict

import (
	"bytes"
	"errors"
	"os"
	"syscall"
	"time"
	"unsafe"

	"golang.org/x/sys/unix"
	"kernel.org/pub/linux/libs/security/libcap/psx"
)

const (
	fsExecute     attribute = unix.LANDLOCK_ACCESS_FS_EXECUTE
	fsWriteFile   attribute = unix.LANDLOCK_ACCESS_FS_WRITE_FILE
	fsReadFile    attribute = unix.LANDLOCK_ACCESS_FS_READ_FILE
	fsReadDir     attribute = unix.LANDLOCK_ACCESS_FS_READ_DIR
	fsRemoveFile  attribute = unix.LANDLOCK_ACCESS_FS_REMOVE_FILE
	fsRemoveDir   attribute = unix.LANDLOCK_ACCESS_FS_REMOVE_DIR
	fsMakeChar    attribute = unix.LANDLOCK_ACCESS_FS_MAKE_CHAR
	fsMakeDir     attribute = unix.LANDLOCK_ACCESS_FS_MAKE_DIR
	fsMakeRegular attribute = unix.LANDLOCK_ACCESS_FS_MAKE_REG
	fsMakeSocket  attribute = unix.LANDLOCK_ACCESS_FS_MAKE_SOCK
	fsMakeFifo    attribute = unix.LANDLOCK_ACCESS_FS_MAKE_FIFO
	fsMakeBlock   attribute = unix.LANDLOCK_ACCESS_FS_MAKE_BLOCK
	fsMakeSymlink attribute = unix.LANDLOCK_ACCESS_FS_MAKE_SYM
	// ABI 2+
	fsRefer attribute = unix.LANDLOCK_ACCESS_FS_REFER
	// ABI 3+
	fsTruncate attribute = unix.LANDLOCK_ACCESS_FS_TRUNCATE
	// ABI 4+
	// todo LANDLOCK_ACCESS_NET_BIND_TCP and LANDLOCK_ACCESS_NET_CONNECT_TCP
)

func init() {
	var m string = "Access"

	if !state.GetBool("disable") {
		u, err := uname()
		if err != nil {
			abort(m, err)
		}

		if bytes.Contains(u.Release, []byte("microsoft")) == true || bytes.Contains(u.Release, []byte("Microsoft")) == true {
			logger.Println("WSL kernels are not supported.")
			Disable()
			return
		}

		abi, err := version()
		if err != nil {
			abort(m, err)
		}
		state.Set("abi", abi)

		if state.GetBool("debug") {
			logger.Println("Landlock ABI: ", abi)
		}

		major, minor, patch, err := u.Kernel()
		if err != nil {
			abort(m, err)
		}

		state.Set("kernel", kernel{major: major, minor: minor, patch: patch})

		if state.GetBool("debug") {
			logger.Printf("Kernel: %v.%v.%v\n", major, minor, patch)
		}

		if major < 6 {
			logger.Println("This running kernel version is unsupported. Please upgrade to 6.x or higher.")
			Disable()
			return
		}
	}
}

// Access tries to behave similar to OpenBSD's unveil syscall.
//
// It limits the filesystem view for that application by defining a path (directory or file) and flags (crwx) as appropiate but independent of the fs permissions. You can call Access() as often as needed, until you call AccessLock() once. A path has to exist prior to the Access() call. If that path is a directory it will enable all filesystem access underneath that path using the given flags.
//
// Beware! The underlaying Landlock works unfortunately unlike the unveil implementation. Know that the first call to Access() does NOT remove visibility of the entire filesystem. You have to call AccessLock() to see that effect. Until then you'll allow full (openat) access by applying unveil/landlock sets to Syscalls(). After AccessLock() you can't take access away by reducing sets/promises like on OpenBSD.
//
// Unlike on unveil on OpenBSD, the landlock implementation on linux passes the restrictions to forked/cloned or execve'd child processes. See SyscallsExec() on linux for more information.
func Access(path string, flags string) error {
	err := caller("Access")
	if err != nil {
		return err
	}

	if state.GetBool("disable") {
		return nil
	}

	if !state.GetBool("isFirstSyscalls") {
		accessSet := (contains(state.GetStrings("syscalls"), "unveil") || contains(state.GetStrings("syscalls"), "landlock"))

		if !accessSet {
			return errors.New("Your previous Syscalls() call is missing the unveil/landlock set to allow the use of Access().")
		}
	}

	if state.GetBool("accessLock") && !state.GetBool("isFirstAccess") {
		return errors.New("restrict.Access was already locked.")
	}

	if state.GetBool("debug") {
		logger.Println("Access: ", path, " Flags: ", flags, " isFirstAccess: ", state.GetBool("isFirstAccess"))
	}

	if !state.GetBool("isFirstSyscalls") {
		for _, ch := range flags {
			switch ch {
			case 'c':
				if !contains(state.GetStrings("syscalls"), "cpath") {
					return errors.New("Your restrict.Access call needs cpath set Syscalls permission.")
				}
			case 'r':
				if !contains(state.GetStrings("syscalls"), "rpath") {
					return errors.New("Your restrict.Access call needs rpath set Syscalls permission.")
				}
			case 'w':
				if !contains(state.GetStrings("syscalls"), "wpath") {
					return errors.New("Your restrict.Access call needs wpath set Syscalls permission.")
				}
			}
		}
	}

	if state.GetBool("isFirstAccess") {
		state.Set("isFirstAccess", false)

		err := Access("/proc/self/task", "r")
		if err != nil {
			logger.Println("Failed to unveil /proc/self/task (r):", err)
		}
	}

	f, err := os.Open(path)
	if err != nil {
		return err
	}
	defer f.Close()

	fi, err := f.Stat()
	if err != nil {
		return err
	}

	p := allow(flags, fi.IsDir())
	a, _ := state.Get("access")
	a = append(a.([]permissions), permissions{date: time.Now(), path: path, attr: p, flags: flags})
	state.Set("access", deduplicate(a.([]permissions)))

	return nil
}

// AccessLock removes the ability to allow Access() to filesystem objects. You must call it once.
// If you call it without calling Access() before, then you will essentially disallow the whole filesystem. (Besides the implicit Access() calls through the other given sets if applicable.)
func AccessLock() error {
	err := caller("AccessLock")
	if err != nil {
		return err
	}

	if state.GetBool("disable") {
		return nil
	}

	if !state.GetBool("accessLock") {
		state.Set("accessLock", true)
	}

	if state.GetBool("accessCerts") {
		if !state.GetBool("rpathIsSet") {
			return errors.New("restrict.AccessCerts() needs 'rpath' as Syscalls() set if Access() is restricted.")
		}
	}

	fd, err := makeRuleset()
	if err != nil {
		return err
	}

	a, _ := state.Get("access")

	for _, p := range a.([]permissions) {
		if state.GetBool("debug") {
			logger.Println(p)
		}

		// skip if no attribute given / esentially a delete after Access
		if p.attr == 0 {
			continue
		}

		var pba unix.LandlockPathBeneathAttr

		fd2, err := syscall.Open(p.path, unix.O_PATH|unix.O_CLOEXEC, 0)
		if err != nil {
			return err
		}
		defer syscall.Close(fd2)

		if state.GetBool("debug") {
			logger.Println(p.path, fd2)
		}

		pba.Allowed_access = uint64(p.attr)
		pba.Parent_fd = int32(fd2)

		if err = addRule(&pba, fd); err != nil {
			return err
		}

		if state.GetBool("debug") {
			logger.Println(p.path, pba)
		}
	}

	if err = prctl(); err != nil {
		if state.GetBool("debug") {
			logger.Println("prctl()")
		}
		return err
	}

	if err = restrictSelf(fd); err != nil {
		if state.GetBool("debug") {
			logger.Println("restrictSelf: ", fd)
		}
		return err
	}

	return nil
}

func allow(flags string, dir bool) attribute {
	var a attribute

	for _, ch := range flags {
		switch ch {
		case 'c':
			if dir {
				if state.GetInt("abi") < 2 {
					a |= fsMakeRegular | fsMakeSocket | fsMakeFifo | fsMakeBlock | fsMakeSymlink | fsMakeDir | fsRemoveFile | fsRemoveDir
				} else {
					a |= fsMakeRegular | fsRefer | fsMakeSocket | fsMakeFifo | fsMakeBlock | fsMakeSymlink | fsMakeDir | fsRemoveFile | fsRemoveDir
				}
			}
		case 'r':
			if dir {
				a |= fsReadFile | fsReadDir
			} else {
				a |= fsReadFile
			}
		case 'w':
			if state.GetInt("abi") < 3 {
				a |= fsWriteFile
			} else {
				a |= fsWriteFile | fsTruncate
			}
		case 'x':
			a |= fsExecute
		}
	}

	if state.GetBool("debug") {
		logger.Printf("allow() attribute: %v / ABI: %v", a, state.GetInt("abi"))
	}

	return a
}

func makeRuleset() (int, error) {
	var attr unix.LandlockRulesetAttr

	if state.GetInt("abi") < 2 {
		attr.Access_fs |= uint64(fsExecute | fsWriteFile | fsReadFile | fsReadDir | fsRemoveFile | fsRemoveDir | fsMakeChar | fsMakeDir | fsMakeRegular | fsMakeSocket | fsMakeFifo | fsMakeBlock | fsMakeSymlink)
	} else if state.GetInt("abi") < 3 {
		attr.Access_fs |= uint64(fsRefer | fsExecute | fsWriteFile | fsReadFile | fsReadDir | fsRemoveFile | fsRemoveDir | fsMakeChar | fsMakeDir | fsMakeRegular | fsMakeSocket | fsMakeFifo | fsMakeBlock | fsMakeSymlink)
	} else {
		attr.Access_fs |= uint64(fsRefer | fsTruncate | fsExecute | fsWriteFile | fsReadFile | fsReadDir | fsRemoveFile | fsRemoveDir | fsMakeChar | fsMakeDir | fsMakeRegular | fsMakeSocket | fsMakeFifo | fsMakeBlock | fsMakeSymlink)
	}

	r0, _, e1 := syscall.Syscall(
		unix.SYS_LANDLOCK_CREATE_RULESET,
		uintptr(unsafe.Pointer(&attr)),
		uintptr(unsafe.Sizeof(unix.LandlockRulesetAttr(attr))),
		0,
	)

	if state.GetBool("debug") {
		logger.Printf("makeRuleset() attr ptr: %v size: %v / ABI: %v", uintptr(unsafe.Pointer(&attr)), unsafe.Sizeof(unix.LandlockRulesetAttr(attr)), state.GetInt("abi"))
		logger.Printf("makeRuleset() Syscall SYS_LANDLOCK_CREATE_RULESET fd: %v error: %v", int(r0), syscall.Errno(e1))
	}

	if e1 == 0 {
		return int(r0), nil
	} else {
		return int(r0), syscall.Errno(e1)
	}
}

func addRule(pba *unix.LandlockPathBeneathAttr, fd int) error {
	_, _, e1 := syscall.Syscall(
		unix.SYS_LANDLOCK_ADD_RULE,
		uintptr(fd),
		uintptr(unix.LANDLOCK_RULE_PATH_BENEATH),
		uintptr(unsafe.Pointer(pba)),
	)

	if e1 == 0 {
		return nil
	} else {
		return syscall.Errno(e1)
	}
}

func prctl() error {
	_, _, e1 := psx.Syscall6(
		syscall.SYS_PRCTL,
		unix.PR_SET_NO_NEW_PRIVS,
		1, 0, 0, 0, 0,
	)

	if e1 == 0 {
		return nil
	} else {
		return syscall.Errno(e1)
	}
}

func restrictSelf(fd int) error {
	_, _, e1 := psx.Syscall3(
		unix.SYS_LANDLOCK_RESTRICT_SELF,
		uintptr(fd),
		0, 0,
	)

	if e1 == 0 {
		return nil
	} else {
		return syscall.Errno(e1)
	}
}

func version() (int, error) {
	r0, _, e1 := syscall.Syscall(
		unix.SYS_LANDLOCK_CREATE_RULESET,
		0,
		0,
		unix.LANDLOCK_CREATE_RULESET_VERSION,
	)

	if e1 != 0 {
		return 0, syscall.Errno(e1)
	}

	if int(r0) <= 0 {
		return 0, errors.New("Landlock is unsupported.")
	}

	return int(r0), nil
}
