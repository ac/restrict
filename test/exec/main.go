package main

import (
	"log"
	"os"
	"os/exec"
	"strings"

	"catinello.eu/restrict"
)

func main() {
	//restrict.Debug()

	//if err := restrict.Syscalls("stdio rpath exec proc landlock"); err != nil {
	if err := restrict.Syscalls("stdio rpath proc landlock"); err != nil {
		log.Fatal(err)
	}

	if err := restrict.Access("/usr/bin/uname", "rx"); err != nil {
		log.Fatal(err)
	}

	if err := restrict.AccessLock(); err != nil {
		log.Fatal(err)
	}

	err := run([]string{"/usr/bin/uname", "-a"})
	if err != nil {
		log.Fatal(err)
	}
}

func run(args []string) error {
	var cmd *exec.Cmd

	if len(args) == 0 {
		os.Exit(1)
	} else if len(args) > 1 {
		cmd = exec.Command(args[0], strings.Join(args[1:], " "))
	} else {
		cmd = exec.Command(args[0])
	}

	cmd.Stdin = os.Stdin
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr

	if err := cmd.Start(); err != nil {
		return err
	}

	return cmd.Wait()
}
