package main

import (
	"log"
	"net"

	"catinello.eu/restrict"
)

func main() {
	// This is supposed to core dump since dns is not allowed!

	//if err := restrict.Syscalls("stdio dns"); err != nil {
	if err := restrict.Syscalls("stdio"); err != nil {
		log.Fatal(err)
	}

	addrs, err := net.LookupHost("example.com")
	if err != nil {
		log.Fatal(err)
	}

	log.Println(addrs)
}
