#!/bin/bash

main() {
  for i in $(ls); do
    if [ -d $i ]; then
      echo -n "$i aborted: "
      cd $i
      local out=$(go run . &> /dev/null)
      cd ..
      journalctl -ae | grep "comm=\"${i}\"" | tail -n 1 | awk '{print $18}'
      #journalctl --system -ae | grep "audit" | grep "go-build" | tail -n 1 | awk '{print $18}'
    fi
  done
}

main "$@"
