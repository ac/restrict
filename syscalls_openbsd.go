// SPDX-FileCopyrightText: 2022 Antonino Catinello
// SPDX-License-Identifier: BSD-3-Clause

//go:build openbsd

package restrict

import (
	"errors"
	"strconv"
	"strings"
	"syscall"

	"golang.org/x/sys/unix"
)

func init() {
	var m string = "Syscalls"

	version, err := syscall.Sysctl("kern.osrelease")
	if err != nil {
		abort(m, err)
	}

	s := strings.Split(version, ".")
	if len(s) != 2 {
		abort(m, errors.New("Could not determine OpenBSD version."))
	}

	major, err := strconv.Atoi(s[0])
	if err != nil {
		abort(m, err)
	}

	minor, err := strconv.Atoi(s[1])
	if err != nil {
		abort(m, err)
	}

	state.Set("kernel", kernel{major: uint16(major), minor: uint16(minor)})
}

// Syscalls is an alias for pledge¹ with some litte filter function to sort out linux only sets.
// All available promises are usable, just like with pledge.
//
// ¹ https://man.openbsd.org/pledge.2
func Syscalls(promises string) error {
	err := caller("Syscalls")
	if err != nil {
		return err
	}

	if state.GetBool("disable") {
		return nil
	}

	if state.GetBool("isFirstSyscalls") {
		state.Set("isFirstSyscalls", false)
	}

	state.Set("syscalls", strings.Fields(syscallsFilter(promises)))

	return unix.PledgePromises(syscallsFilter(promises))
}

// SyscallsExec is an alias for pledge's execpromises with some litte filter function to sort out linux only sets.
// All available promises are usable, just like with pledge.
func SyscallsExec(promises string) error {
	err := caller("SyscallsExec")
	if err != nil {
		return err
	}

	if state.GetBool("disable") {
		return nil
	}

	return unix.PledgeExecpromises(syscallsFilter(promises))
}

func syscallsFilter(promises string) string {
	var pp string

	f := strings.Fields(promises)

	if contains(f, "dns") {
		state.Set("dnsIsSet", true)
	} else {
		state.Set("dnsIsSet", false)
	}

	if contains(f, "rpath") {
		state.Set("rpathIsSet", true)
	} else {
		state.Set("rpathIsSet", false)
	}

	for _, p := range f {
		v, ok := syscallsMap(p)
		if ok {
			if len(v) > 0 {
				if len(pp) > 0 && len(v) > 0 {
					pp = pp + " " + v
				} else {
					pp = v
				}
			}
		} else {
			if len(pp) > 0 {
				pp = pp + " " + p
			} else {
				pp = p
			}
		}
	}

	dedupe := make(map[string]bool)
	var r string

	for _, s := range strings.Fields(pp) {
		if _, ok := dedupe[s]; !ok {
			dedupe[s] = true
			if len(r) > 0 {
				r = r + " " + s
			} else {
				r = s
			}
		}
	}

	return r
}

func subset(initial, next []string) bool {
	println("! This is a mock-up to satisfy tests returning always true.")
	return true
}

func syscallsMap(set string) (string, bool) {
	s := map[string]string{
		"landlock":     "unveil",
		"capabilities": "",
		"namespaces":   "",
	}

	v, ok := s[set]
	return v, ok
}
